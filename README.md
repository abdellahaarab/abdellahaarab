# Hi, I'm Abdellah 👋

I'm a  Web FullStack Developer from Morocco passionate about software development.

## Find me on the web 🌍
- 🌐 Website [animexdrg.com](https://animexdrg.com/)
- 👨‍💻 My Applications [My Applications](https://play.google.com/store/apps/dev?id=8132444417927082763)
- 📑 Author on [Milkomida-logy](https://deepxculture.wordpress.com/)




</br>

# Some of my Projects 👨‍💻

* [Anime XDRG](https://animexdrg.com/)

  Anime XDRG is the best place for anime enthusiasts! Anime news, guides, reviews, recommendations, character bios, we have it all.

* [Youtube Downloader](https://tube.animexdrg.com/)

  Fast and free Youtube video downloader.




<center >
<span style="margin-right:50px"></span>

## Languages and Tools 🛠️

<span style="margin-right:20px"></span>

<table border="0">
<tbody>

<tr>
  <td>
      <img alt="Visual Studio Code" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Tools/vscode.png" />
  </td>
  <td>
      <img alt="Pycharm" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Tools/pycharm.png" />
  </td>
  <td>
    <img alt="Git" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Tools/git.png" />
  </td>
  <td>
    <img alt="Python" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Languages/python.png" />
  </td>
  <td>
    <img alt="HTML5" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Languages/html.png" />
  </td>
</tr>

<tr>
  <td>
   <img alt="Css" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Languages/css.png" />
  </td>
  <td>
    <img alt="Bootstrap" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Languages/bootstrap.jpg" />
  </td>
  <td>
    <img alt="Javascript" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Languages/javascript.png" />
  </td>
  <td>
    <img alt="Django" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Languages/dart.jpg" />
  </td>
  <td>
    <img alt="Node.js" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Languages/flutter.png" />
  </td>
</tr>

<tr>
 <td>
    <img alt="Node.js" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Languages/nodejs.png" />
 </td>
 <td>
    <img alt="Php" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Languages/php.png" />
 </td>
 <td>
    <img alt="Django" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Languages/django.jpg" />
 </td>
 <td>
   <img alt="React JS" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Languages/react.png" />
 </td>
 <td>
    <img alt="Markdown" align="left" width="26px" style="margin-right:15px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Languages/markdown.png" />
 </td>
</tr>

</tbody>
</table>

<span style="margin-right:20px"></span>


<div align="center">

##  GitHub Stats

![Abdellah's GitHub stats](https://github-readme-stats.vercel.app/api?username=abdellahaarab&show_icons=true&bg_color=30,e96443,904e95&title_color=fff&text_color=fff)

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=abdellahaarab&layout=compact)](https://github.com/abdellahaarab/github-readme-stats)



![](https://github-readme-streak-stats.herokuapp.com/?user=abdellahaarab&hide_border=false)

</div>


<span style="margin-right:50px"></span>

##  Connect with me 🔗 

  <a href="https://twitter.com/deepxculture">
      <img alt="Abdellah Aarab | Twitter"  width="26px" style="margin-right:25px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Media/twitter.svg" />
    </a>
  <a href="https://gitlab.com/abdellahaarab">
    <img alt="Abdellah Aarab | Gitlab"  width="26px" style="margin-right:25px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Media/gitlab.svg" />
  </a>
  <a href="https://github.com/abdellahaarab">
    <img alt="Abdellah Aarab | Github"  width="26px" style="margin-right:25px" src="https://gitlab.com/abdellahaarab/abdellahaarab/-/raw/main/assets/Media/github.png" />
  </a>
</center>

